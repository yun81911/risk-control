# 新手教程

### 序

前端由开源项目 `iczer/vue-antd-admin` 删改而来，地址：https://gitee.com/iczer/vue-antd-admin

### 关于前端

如果你只是想要写一些组件和页面，你需要按顺序学习如下知识，链接只是参考网址可以根据自己喜好选择参考资料。

1. ES6：https://es6.ruanyifeng.com     (只需看完1~11章节)

2. VUE：https://cn.vuejs.org/  （前端框架）

3. 想要看懂本项目前端代码，学习完VUE之后还需要了解VUE脚手架相关知识

4. Ant Design of Vue：https://www.antdv.com/docs/vue/introduce-cn/  （此为UI组件库）

5. VUEX：https://vuex.vuejs.org/zh/  （状态管理）

   

如果你想修改本项目任意前端代码，除了上述要求之外，你还需要如下知识。

   

6. ES6：https://es6.ruanyifeng.com     (16~20章节)

7. Vue Router：https://router.vuejs.org/zh/  （路由）
 
8. 一些简单的Webpack的知识


