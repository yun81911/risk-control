import TabsView from '@/layouts/tabs/TabsView'
import BlankView from '@/layouts/BlankView'

// 路由配置   只有在异步路由未开启的情况下有用
const options = {
  routes: [
    {
      path: '/login',
      name: '登录页',
      component: () => import('@/pages/login')
    },
    {
      path: '*',
      name: '404',
      component: () => import('@/pages/exception/404'),
    },
    {
      path: '/403',
      name: '403',
      component: () => import('@/pages/exception/403'),
    },
    {
      path: '/tab',
      name: '标签页',
      component: TabsView,
      children: [
        {
          path: 'builder/:objectId',
          name: '构建',
          meta: {
            page: {
              closable: true
            }
          },
          component: () => import('@/pages/systemManage/tabBuilder'),
        }
      ],
    },
    {
      path: '/',
      name: '首页',
      component: TabsView,
      redirect: '/login',
      children: [
        {
          path: 'systemManage',
          name: '系统管理',
          meta: {
            icon: 'setting'
          },
          component: BlankView,
          children: [
            {
              path: 'modelManage',
              name: '模型管理',
              meta: {
                page: {
                  closable: true
                }
              },
              component: () => import('@/pages/systemManage/modelManage'),
            }
          ]
        }
      ]
    },
  ]
}

export default options
