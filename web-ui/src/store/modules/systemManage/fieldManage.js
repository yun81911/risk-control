import {addField, deleteById, queryAllField, update} from '@/services/systemManage/fieldManage'
import {storePrompt} from '@/store/modules/commonStore'

export default {
  namespaced: true,
  state: {
    tableData: [],
    showTableDada: [],
    currentKey: undefined,
    popupVisible: false,
    popupTitle: undefined,
    currentEditKey: undefined,
    editData: null
  },
  actions: {
    async queryAllField({commit, state}, payload) {
      await queryAllField({...payload, modelId: state.currentKey}).then((obj) => {
        let {data, success} = obj.data
        if (success) {
          commit("setState", {name: 'tableData', value: data})
          commit("setState", {name: 'showTableDada', value: data})
        }
      })
    },
    async deleteById({commit, state}, payload) {
      await deleteById(payload).then((obj) => {
        storePrompt(obj.data)
      })
    },
    async update({commit, state}, payload) {
      await update({...payload, objectId: state.currentEditKey, modelId: state.currentKey}).then((obj) => {
        storePrompt(obj.data)
      })
    },
    async addField({commit, state}, payload) {
      await addField({...payload, modelId: state.currentKey}).then((obj) => {
        storePrompt(obj.data)
      })
    },
  }
}
