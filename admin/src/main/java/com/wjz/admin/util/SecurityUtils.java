package com.wjz.admin.util;

import com.wjz.admin.config.security.service.SysAccount;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author wjz
 * @date 2020/12/2 17:16
 * @Description 安全服务工具类
 */
@Component
public class SecurityUtils {

    /**
     * 获取Authentication
     */
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }


    /**
     * 获取当前用户id
     */
    public Long getCurrentUserId() {
        SysAccount principal = (SysAccount) getAuthentication().getPrincipal();
        return principal.getUserId();
    }

    /**
     * 获取当前用户简略信息
     */
    public SysAccount getSimplenessCurrentUser() {

        SysAccount principal = (SysAccount) getAuthentication().getPrincipal();
        return new SysAccount().setAlias(principal.getAlias())
                .setUsername(principal.getUsername())
                .setRoles(principal.getRoles())
                .setObjectId(principal.getObjectId());
    }
}
