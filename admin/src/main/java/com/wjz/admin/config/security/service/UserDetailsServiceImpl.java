package com.wjz.admin.config.security.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wjz.admin.mapper.SysAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户验证处理
 *
 * @author wjz
 * @date 2020/11/21 22:10
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private SysAccountMapper accountMapper;

    /**
     * 用户验证
     **/
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username == null || "".equals(username)) {
            throw new RuntimeException("用户名不能为空");
        }
        SysAccount account = accountMapper.selectOne(new QueryWrapper<SysAccount>().eq("username", username));
        if (account == null) {
            throw new UsernameNotFoundException("该用户不存在！");
        }
//        account.setRoles(accountMapper.selectRoleById(account.getObjectId()));
        return account;
    }
}
