package com.wjz.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.entity.vo.PreItemVo;
import com.wjz.commons.service.PreItemApiService;
import com.wjz.commons.util.ResultTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author wjz
 * @date 2021/7/18 15:33
 */
@RestController
@RequestMapping("/preItem")
public class PreItemApiController {

    @Autowired
    private PreItemApiService preItemApiService;

    @PostMapping
    public AjaxResult save(@RequestBody PreItemVo preItemVo) {
        return ResultTool.success(preItemApiService.save(preItemVo.setCreateTime(new Date())));
    }

    @PutMapping
    public AjaxResult update(@RequestBody PreItemVo preItemVo) {
        return ResultTool.success(preItemApiService.updateById(preItemVo.setUpdateTime(new Date())));
    }

    @GetMapping
    public AjaxResult queryAllPreItem(Long modelId) {
        return ResultTool.success(preItemApiService.list(new LambdaQueryWrapper<PreItemVo>().eq(PreItemVo::getModelId, modelId)));
    }

    @DeleteMapping("/deleteById/{objectId}")
    public AjaxResult deleteById(@PathVariable Long objectId) {
        return ResultTool.success(preItemApiService.removeById(objectId));
    }
}
