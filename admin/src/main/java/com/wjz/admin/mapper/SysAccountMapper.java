package com.wjz.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjz.admin.config.security.service.SysAccount;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjz
 * @date 2020/11/21 22:08
 */
@Mapper
public interface SysAccountMapper extends BaseMapper<SysAccount> {

}
