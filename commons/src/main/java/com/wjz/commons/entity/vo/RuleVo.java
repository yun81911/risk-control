package com.wjz.commons.entity.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wjz
 * @date 2021/9/8 20:56
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "RuleVo", description = "规则实体")
public class RuleVo implements Serializable {

    @TableId
    private Long objectId;

    private Long modelId;

    private Long activationId;

    private String name;

    private String label;

    private String scripts;

    private Integer initScore;

    private Integer baseNum;

    private String operator;

    private String abstractionName;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    private Integer rate;

    private Integer max;

    private String ruleDefinition;
}
