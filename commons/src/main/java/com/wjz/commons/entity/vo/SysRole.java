package com.wjz.commons.entity.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wjz
 * @date 2020/11/20 10:50
 * 角色
 */
@Data
@EqualsAndHashCode
@Accessors(chain = true)
@ApiModel(value = "SysRole", description = "角色")
public class SysRole implements Serializable {

    @TableId
    private Long objectId;

    private String tag;

    private String name;

    private Integer deleted;

    private String description;

    private Date createdDate;

    private Long createdBy;

    private Date lastUpdateDate;

    private Long lastUpdateBy;
}
