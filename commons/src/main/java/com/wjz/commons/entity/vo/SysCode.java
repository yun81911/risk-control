package com.wjz.commons.entity.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * @author wjz
 * @Date 2020-12-22
 */

@Data
@EqualsAndHashCode
@Accessors(chain = true)
@ApiModel(value = "SysCode", description = "字典")
public class SysCode implements Serializable {

    @TableId
    private Long objectId;

    private String name;

    private String tag;

    private Integer deleted;

    private Integer orderNo;

    private String description;

    private Long parentId;

    private Date createdDate;

    private Long createdBy;

    private Date lastUpdateDate;

    private Long lastUpdateBy;
}
