package com.wjz.commons.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjz.commons.entity.vo.AbstractionVo;
import com.wjz.commons.mapper.AbstractionVoMapper;
import org.springframework.stereotype.Service;

/**
 * @author wjz
 * @date 2021/9/5 21:58
 */
@Service
public class AbstractionApiService extends ServiceImpl<AbstractionVoMapper, AbstractionVo> {
}
