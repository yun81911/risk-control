package com.wjz.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjz.commons.entity.vo.RuleVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjz
 * @date 2021/9/8 20:59
 */
@Mapper
public interface RuleVoMapper extends BaseMapper<RuleVo> {
}
