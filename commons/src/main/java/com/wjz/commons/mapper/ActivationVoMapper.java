package com.wjz.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjz.commons.entity.vo.ActivationVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjz
 * @date 2021/9/7 17:07
 */
@Mapper
public interface ActivationVoMapper extends BaseMapper<ActivationVo> {
}
