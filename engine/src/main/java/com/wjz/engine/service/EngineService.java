package com.wjz.engine.service;

import com.alibaba.fastjson.JSONObject;
import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.entity.vo.ModelVo;
import com.wjz.commons.enums.MasterConstants;
import com.wjz.commons.mongo.EntityService;
import com.wjz.commons.service.ModelApiService;
import com.wjz.commons.util.ResultTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wjz
 * @date 2021/9/12 17:19
 */
@Service
public class EngineService {

    @Autowired
    private ModelApiService modelApiService;

    @Autowired
    private EntityService entityService;

    public AjaxResult uploadInfo(String modelGuid, String reqId, JSONObject jsonInfo) {

        Map<String, Map<String, ?>> context = new HashMap<>();
        ModelVo model = modelApiService.getModelByGuid(Long.getLong(modelGuid));
        //1.check
        if (model == null) {
            return ResultTool.fail("模型不存在!");
        }
        if (!model.getStatus().equals(MasterConstants.ACTIVE)) {
            return ResultTool.fail("模型未激活!");
        }
        //2.预处理
        //3.保存数据
        entityService.saveEntity(model, jsonInfo.toJSONString(), true);
        //4.执行分析


        return ResultTool.success();
    }
}
